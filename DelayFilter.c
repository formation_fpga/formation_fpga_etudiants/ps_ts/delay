/*
DelayFilter.c
 *
 *  Created on: 30 avr. 2020
 *  modified on : 10/07/2020
 *  Author: JB
 */



#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "PmodAD1.h"
#include "PmodDA1.h"
#include "sleep.h"
#include "xil_cache.h"
#include "xil_io.h"
#include "xil_types.h"
#include "xparameters.h"
#include"xgpio.h"
#include "xstatus.h"
#include "xintc.h"
#include "xil_exception.h"
#include "xtmrctr.h"

// Elements de definition TIMER

#define INTC_DEVICE_ID             XPAR_INTC_0_DEVICE_ID
#define INTC_DEVICE_INT_ID         XPAR_INTC_0_TMRCTR_0_VEC_ID
#define TMRCTR_DEVICE_ID        XPAR_TMRCTR_0_DEVICE_ID
#define TIMER_CNTR_0             0
#define RESET_VALUE                 0xFFFFF000

// elements de definition ADC DAC
#define quantumDAC 0.012941176471   // DAC 8 BITS
#define quantumADC 0.000805860806   // ADC 12 BITS

// definition des variables globales
static XIntc InterruptController;
static XTmrCtr TimerCounterInst;
//volatile static int InterruptProcessed = FALSE;
PmodAD1 myDevice;
PmodDA1 myDevice1;
XGpio gpio;
XGpio marqueur;
AD1_RawData RawData;
AD1_PhysicalData PhysicalData;

// prototypes de fonction
void InitADCDAC();
void TraitementSignal();
void ArretProgramme();
void EnableCaches();
void DisableCaches();
int InitialisationTimer();
int SetUpInterruptSystem(XIntc *XIntcInstancePtr, XTmrCtr *XTmrInstancePtr);
void DeviceDriverHandler(void *CallbackRef);

// Prototypes fonctions filtres signaux
u32 Retard (u32 *XN0);

int main() {


/**** INITIALISATION DES PERIPHERIQUES GPIO ****/
	XGpio_Initialize(&gpio, 0);  // Initialisation du GPIO LEDs
	XGpio_Initialize(&marqueur,XPAR_MARQUEUR_DEVICE_ID); // Initialisation du GPIO MARQUEUR
	XGpio_SetDataDirection(&gpio, 2, 0x00000000); // Paramtrage des Leds en sortie
/*********************************/
		InitADCDAC();
	   InitialisationTimer();  // autorisation timer
	   while(1){
	   }
	   ArretProgramme();
   return 0;
}


/**** ROUTINE D'INTERRUPTION*****/
void DeviceDriverHandler(void *CallbackRef)

{
	XGpio_DiscreteWrite(&marqueur,1, 1);  // marqueur
	TraitementSignal(); // appel de la fonction traitement du signal
	/**** reinitialisation du timer *****/
    XTmrCtr_SetResetValue(&TimerCounterInst, TIMER_CNTR_0, RESET_VALUE);
    XTmrCtr_Start(&TimerCounterInst, TIMER_CNTR_0);
	XGpio_DiscreteWrite(&marqueur,1,0); // marqueur
}

/*********Implmentation d'un filtre  *********************/
void TraitementSignal() {
  u32 X0,Y0;
  X0=AD1_GetSample(&myDevice, &RawData); 		// Echantillonnage et conversion ADC
  Y0= Retard(&X0);								// filtrage
  DA1_WriteIntegerValue(&myDevice1,Y0); 		// echantillonnage, conversion et ecriture DAC
}

  u32 Retard (u32 *XN0)
  {
	u32 YN0,XN3,XN2,XN1; // nombre de varaiable dépendant du nombre de retards
	YN0=XN1;
	XN1=*XN0*(quantumADC/quantumDAC);
	return YN0;
}

/***** Initialisation ADC et DAC************/
void InitADCDAC() {
   EnableCaches();
   AD1_begin(&myDevice, XPAR_PMODAD1_0_AXI_LITE_SAMPLE_BASEADDR);
   DA1_begin(&myDevice1, XPAR_PMODDA1_0_AXI_LITE_SPI_BASEADDR);
   usleep(1); // 1 us (minimum)
}


/***** Fonction arrt programme ****/
void ArretProgramme() {
	DA1_end(&myDevice1);
   DisableCaches();
}
/*************** paramtrages TIMER *******************************/
int InitialisationTimer()
{
	int Status;

XIntc_Initialize(&InterruptController, INTC_DEVICE_ID);
XIntc_SelfTest(&InterruptController);
XTmrCtr_Initialize(&TimerCounterInst, TMRCTR_DEVICE_ID);
XTmrCtr_SelfTest(&TimerCounterInst,TIMER_CNTR_0);
SetUpInterruptSystem(&InterruptController,&TimerCounterInst);

XTmrCtr_SetHandler(&TimerCounterInst,DeviceDriverHandler,&TimerCounterInst);
XTmrCtr_SetOptions(&TimerCounterInst, TIMER_CNTR_0, XTC_INT_MODE_OPTION);
XTmrCtr_SetResetValue(&TimerCounterInst, TIMER_CNTR_0, RESET_VALUE);
XTmrCtr_Start(&TimerCounterInst, TIMER_CNTR_0);

return 0;
}

int SetUpInterruptSystem(XIntc *XIntcInstancePtr, XTmrCtr *XTmrInstancePtr)
{


XIntc_Connect(XIntcInstancePtr, INTC_DEVICE_INT_ID,(XInterruptHandler)DeviceDriverHandler,(void *)XTmrInstancePtr);

XIntc_Start(XIntcInstancePtr, XIN_REAL_MODE);
XIntc_Enable(XIntcInstancePtr, INTC_DEVICE_INT_ID);

Xil_ExceptionInit();

Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,
					(Xil_ExceptionHandler)XIntc_InterruptHandler,
					XIntcInstancePtr);

Xil_ExceptionEnable();

return XST_SUCCESS;

}

void EnableCaches() {
#ifdef __MICROBLAZE__
#ifdef XPAR_MICROBLAZE_USE_ICACHE
   Xil_ICacheEnable();
#endif
#ifdef XPAR_MICROBLAZE_USE_DCACHE
   Xil_DCacheEnable();
#endif
#endif
}

void DisableCaches() {
#ifdef __MICROBLAZE__
#ifdef XPAR_MICROBLAZE_USE_DCACHE
   Xil_DCacheDisable();
#endif
#ifdef XPAR_MICROBLAZE_USE_ICACHE
   Xil_ICacheDisable();
#endif
#endif
}


